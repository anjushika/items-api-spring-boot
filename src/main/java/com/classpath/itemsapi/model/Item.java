package com.classpath.itemsapi.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode (of="itemId")
@Builder
@Entity
@Table(name = "items")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long itemId;

    @NotEmpty(message = "Item name cannot be empty")
    private String name;

    @Max(value = 250000, message = "Item price cannot be more than 250K")
    @Min(value = 100000, message = "Item price cannot be less than 100K")
    private double price;


    @NotEmpty(message = "description of item is mandatory")
    private String desc;
}