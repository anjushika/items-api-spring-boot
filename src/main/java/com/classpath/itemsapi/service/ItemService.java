package com.classpath.itemsapi.service;

import com.classpath.itemsapi.model.Item;

import java.util.Set;

public interface ItemService {

    public Item saveItem(Item item);

    public Set<Item> fetchItems ();

    public Item fetchItemById(long itemId);

    public void deleteItemById(long itemId);
}