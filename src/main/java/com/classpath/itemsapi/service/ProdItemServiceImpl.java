package com.classpath.itemsapi.service;

import com.classpath.itemsapi.model.Item;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
//@Profile("prod")
public class ProdItemServiceImpl implements ItemService {

    @Override
    public Item saveItem(Item item) {
        return null;
    }

    @Override
    public Set<Item> fetchItems() {
        return null;
    }

    @Override
    public Item fetchItemById(long itemId) {
        return null;
    }

    @Override
    public void deleteItemById(long itemId) {

    }
}