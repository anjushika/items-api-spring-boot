package com.classpath.itemsapi.config;

import com.classpath.itemsapi.service.DataSource;
import com.classpath.itemsapi.service.PaymentGateway;
import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.boot.cloud.CloudPlatform;
import org.springframework.boot.system.JavaVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class ApplicationConfig {

    @Bean
    @ConditionalOnProperty(prefix = "app", name = "paymentgateway.enabled", havingValue = "true", matchIfMissing = true)
    public PaymentGateway paymentGateway(){
        return new PaymentGateway();
    }

    @Bean("h2DataSource")
    @ConditionalOnBean(name="paymentGateway")
    public DataSource dataSource(){
        return new DataSource();
    }

    @Bean("mysqlDataSource")
    @ConditionalOnMissingBean(name="paymentGateway")
    public DataSource customDataSource(){
        return new DataSource();
    }
    @Bean("awsDataSource")
    @ConditionalOnCloudPlatform(CloudPlatform.KUBERNETES)
    public DataSource awsRDSDatasource(){
        return new DataSource();
    }

    @Bean("java8DataSource")
    @ConditionalOnJava(JavaVersion.EIGHT)
    public DataSource java8Datasource(){
        return new DataSource();
    }

    @Bean("java11DataSource")
    @ConditionalOnJava(JavaVersion.ELEVEN)
    public DataSource java11Datasource(){
        return new DataSource();
    }
}