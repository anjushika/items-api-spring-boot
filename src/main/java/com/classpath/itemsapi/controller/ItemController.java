package com.classpath.itemsapi.controller;

import com.classpath.itemsapi.model.Item;
import com.classpath.itemsapi.service.ItemService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/")
@AllArgsConstructor
@Slf4j
public class ItemController {

    private  ItemService itemService;
    private  ApplicationContext applicationContext;

    @GetMapping("/items")
    public Set<Item> fetchAllItems(){
        log.info("Came inside the fetchAllItems {}", "all");
        return this.itemService.fetchItems();
    }


    @PostMapping("/items")
    public Item saveItem(@RequestBody @Valid Item item){
        return this.itemService.saveItem(item);
    }

    @GetMapping("/items/{itemId}")
    public Item fetchItemById(@PathVariable("itemId") long itemId){
        return this.itemService.fetchItemById(itemId);
    }

    @DeleteMapping("items/{itemId}")
    public void deleteItemById(@PathVariable("itemId") long itemId){
        this.itemService.deleteItemById(itemId);
    }

    @GetMapping("/beans")
    public Set<String> beans(){
        return new HashSet<>(Arrays.asList(applicationContext.getBeanDefinitionNames()));
    }

}