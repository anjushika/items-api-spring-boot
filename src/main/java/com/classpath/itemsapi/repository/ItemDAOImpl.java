package com.classpath.itemsapi.repository;

import com.classpath.itemsapi.model.Item;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

//@Repository
public class ItemDAOImpl implements ItemDAO {

    private static Set<Item> items = new HashSet<>(Arrays.asList(
            Item.builder().itemId(12).name("IPad").price(56_000).build(),
            Item.builder().itemId(14).name("MacBook-Pro").price(2_56_000).build()
    ));

    @Override
    public Item save(Item item) {
        System.out.println(" Inside the save method of ItemDAOImpl::::");
        return item;
    }

    @Override
    public Set<Item> fetchItems() {
        return items;
    }

    @Override
    public void deleteItem(long itemId) {

    }

    @Override
    public Item update(long itemId, Item item) {
        return null;
    }

    @Override
    public Item fetchItemById(long itemId) {
        return null;
    }
}